package com.pcf.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PcfTutorialApplication {

	public static void main(String[] args) {
		SpringApplication.run(PcfTutorialApplication.class, args);
	}

}
