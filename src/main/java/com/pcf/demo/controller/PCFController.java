package com.pcf.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PCFController {
	
	@GetMapping("/hello")
	public String sayHello() {
		return "Hello Koushik!!";
	}

}
